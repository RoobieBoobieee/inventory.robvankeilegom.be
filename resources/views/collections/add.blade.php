<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add item') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <form method="POST" action="{{ route('save-collection') }}">
                        @csrf

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />

                        <!-- Name -->
                        <div>
                            <x-label for="name" :value="__('Name')" class="mt-4" />

                            <x-translatable id="name" class="block w-full" type="text" name="name" :value="old('name')" required autofocus />
                        </div>

                        <!-- Description -->
                        <div>
                            <x-label for="" :value="__('Description')" class="mt-4" />

                            <x-translatable id="description" class="block w-full" type="text" name="description" :value="old('description')" autofocus />
                        </div>

                        <!-- Parent -->
                        <div>
                            <x-label for="" :value="__('Parent')" class="mt-4" />

                            <x-select id="parent" class="block w-full" name="parent" :value="old('parent')" autofocus>
                                <option value>{{ __('None') }}</option>
                                @foreach($collections as $uuid => $name)
                                    <option value="{{ $uuid }}">{{ $name }}</option>
                                @endforeach

                            </x-select>
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <x-button>
                                {{ __('Save') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
