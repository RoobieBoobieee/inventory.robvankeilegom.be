<x-app-layout>
    <x-slot name="header">
        <div class="flex">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight flex-grow">
                {{ __('Layers') }}

            </h2>
            <a href="{{ route('add-layer') }}">
                {{ __('Add a new layer') }}
            </a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <table class="table-auto">
                        <thead>
                            <tr>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Description') }}</th>
                                <th>{{ __('Actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($layers as $layer)
                                <tr>
                                    <td>{{ $layer->name }}</td>
                                    <td>{{ $layer->description }}</td>
                                    <td>
                                        <a href="{{ route('data-layer', [ $layer->uuid ]) }}">
                                            {{ __('Edit layout') }}
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
