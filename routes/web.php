<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\LayerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CollectionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    Route::prefix('item')->group(function () {
        Route::get('/list', [ItemController::class, 'list'])->name('items-list');

        Route::get('/add', [ItemController::class, 'add'])->name('add-item');
        Route::post('/add', [ItemController::class, 'save'])->name('save-item');
    });

    Route::prefix('collection')->group(function () {
        Route::get('/list', [CollectionController::class, 'list'])->name('collections-list');

        Route::get('/add', [CollectionController::class, 'add'])->name('add-collection');
        Route::post('/add', [CollectionController::class, 'save'])->name('save-collection');
    });

    Route::prefix('layer')->group(function () {
        Route::get('/list', [LayerController::class, 'list'])->name('layers-list');

        Route::get('/add', [LayerController::class, 'add'])->name('add-layer');
        Route::post('/add', [LayerController::class, 'save'])->name('save-layer');

        Route::get('/data/{layer}', [LayerController::class, 'data'])->name('data-layer');
    });
});

require __DIR__ . '/auth.php';
