<?php

namespace App\Scopes;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;

class ScopedByUser implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if (! Auth::check()) {
            throw new \RuntimeException('No user defined');
        }

        $builder->where($model->getQualifiedOwnerIdColumn(), Auth::user()->id);
    }

    /**
     * Extend the query builder with the needed functions.
     */
    public function extend(Builder $builder): void
    {
        $this->addWithoutOwnerScope($builder);
    }

    /**
     * Get the "owner_id" column for the builder.
     */
    protected function getOwnerIdColumn(Builder $builder): string
    {
        if (count((array) $builder->getQuery()->joins) > 0) {
            return $builder->getModel()->getQualifiedOwnerIdColumn();
        }

        return $builder->getModel()->getOwnerIdColumn();
    }

    /**
     * Add the without-user-scope extension to the builder.
     */
    protected function addWithoutOwnerScope(Builder $builder): void
    {
        $builder->macro('withoutOwnerScope', function (Builder $builder) {
            return $builder->withoutGlobalScope($this);
        });
    }
}
