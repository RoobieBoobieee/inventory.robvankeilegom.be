<?php

namespace App\Rules;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Validation\Rule;

class EfficientUuidExists implements Rule
{
    protected string $table;

    /**
     * Create a new rule instance.
     */
    public function __construct(string $table)
    {
        $this->table = $table;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // If a table is given. Check if the uuid exists
        return ! DB::table($this->table)->whereUuid($value)->exists();
    }

    /**
     * Get the validation error message.
     */
    public function message(): string
    {
        return 'Uuid does not exist';
    }
}
