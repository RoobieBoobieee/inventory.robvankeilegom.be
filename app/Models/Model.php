<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Base;
use RoobieBoobieee\Translatables\Models\Translation;

abstract class Model extends Base
{
    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @param  null|string  $field
     *
     * @return null|\Illuminate\Database\Eloquent\Model
     */
    public function resolveRouteBinding($value, $field = null)
    {
        return $this->whereUuid($value, $field)->firstOrFail();
    }

    /**
     * @param string $column column name for the value
     * @param string $key if not given, id is used
     * @param null|mixed $collection
     *
     * @return array
     */
    public static function dictionary($column, $key = 'id', $collection = null)
    {
        if (! $collection) {
            $collection = static::all();
        }

        return $collection
            ->keyBy($key)
            ->map(function ($record) use ($column) {
                $value = $record->getAttributeValue($column);

                if ($value instanceof Translation) {
                    return (string) $value;
                }

                return $value;
            })
            ->sort();
    }
}
