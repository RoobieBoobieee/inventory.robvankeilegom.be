<?php

namespace App\Models;

use App\Traits\BelongsToUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cupboard extends Model
{
    use HasFactory;
    use BelongsToUser;
}
