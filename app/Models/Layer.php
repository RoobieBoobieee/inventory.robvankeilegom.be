<?php

namespace App\Models;

use App\Casts\Json;
use App\Traits\BelongsToUser;
use Dyrynda\Database\Casts\EfficientUuid;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use RoobieBoobieee\Translatables\Models\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use RoobieBoobieee\Translatables\Traits\HasTranslations;

/**
 * @property Translation $name
 * @property Translation $description
 */
class Layer extends Model
{
    use HasFactory;
    use GeneratesUuid;
    use SoftDeletes;
    use BelongsToUser;
    use HasTranslations;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid' => EfficientUuid::class,
        'data' => Json::class,
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    protected $localizable = [
        'name', 'description',
    ];
}
