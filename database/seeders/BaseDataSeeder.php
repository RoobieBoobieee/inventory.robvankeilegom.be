<?php

namespace Database\Seeders;

use App\Models\Item;
use App\Models\User;
use App\Models\Collection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class BaseDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $user = User::first();
        Auth::login($user);

        foreach ($this->data() as $record) {
            $collectionData = $record;
            unset($collectionData['items']);

            $collection = new Collection($collectionData);
            $collection->save();

            foreach ($record['items'] as $itemData) {
                $item = new Item($itemData);
                // $item->collection()->associate($collection);
                $item->save();
            }
        }
    }

    private function data(): array
    {
        return [
            [
                'name' => [
                    'nl' => 'Electronica',
                    'en' => 'Electronics',
                ],
                'items' => [
                    [
                        'name' => [
                            'nl' => 'Weerstand',
                            'en' => 'Resistor',
                        ],
                    ],
                ],
            ],
        ];
    }
}
