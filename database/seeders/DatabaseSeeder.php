<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        if ('local' === config('app.env')) {
            User::create([
                'email'    => 'info@robvankeilegom.be',
                'name'     => 'Rob',
                'password' => Hash::make('password'),
            ]);
        }

        $this->call(BaseDataSeeder::class);
    }
}
