<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->id();
            $table->efficientUuid('uuid');

            $table->foreignId('owner_id')->constrained('users');
            $table->foreignId('parent_id')->nullable()->constrained('collections');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('collections_translations', function (Blueprint $table) {
            $table->translations('collections');

            $table->string('name')->nullable();
            $table->longText('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('collections_translations');
        Schema::dropIfExists('collections');
    }
}
