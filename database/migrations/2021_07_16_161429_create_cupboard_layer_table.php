<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCupboardLayerTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('cupboard_layer', function (Blueprint $table) {
            $table->id();

            $table->foreignId('cupboard_id')->constrained();
            $table->foreignId('layer_id')->constrained();

            $table->unsignedTinyInteger('sequence');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('cupboard_layer');
    }
}
