<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->efficientUuid('uuid');

            $table->foreignId('owner_id')->constrained('users');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('items_translations', function (Blueprint $table) {
            $table->translations('items');

            $table->string('name')->nullable();
            $table->longText('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('items_translations');
        Schema::dropIfExists('items');
    }
}
