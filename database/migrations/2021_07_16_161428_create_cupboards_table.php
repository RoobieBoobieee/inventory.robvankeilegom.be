<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCupboardsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('cupboards', function (Blueprint $table) {
            $table->id();
            $table->efficientUuid('uuid');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('cupboards_translations', function (Blueprint $table) {
            $table->translations('cupboards');

            $table->string('name');
            $table->longText('description');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('cupboards_translations');
        Schema::dropIfExists('cupboards');
    }
}
